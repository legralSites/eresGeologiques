/*!
gestion d'une frise specialiser pour afficher les eres geologiques
Frise au limite variable
*/
friseB=null;
function createFriseB()
{
friseB=new Tfrise({
	'idHTML':'friseB_support'
	,'supportRegles':'friseB_supportRegles'
	,'supportDatas':'friseB_supportDatas'
	,'bandeModele':{'date_deb': {'an': -4.6*ut.Milliard},'date_fin': {'an': 4000},'left_marge': 0,'longueur': 1000,'top': 0,'hauteur': 30}
//	,'bandeModele':friseA.bandeModele
	});

friseB.initialisation=function()
	{
	// --- auteurs - creation de la bande reference/model  --- //
	showActivity("creation de la friseB",1);

	this.evtStyle=eresGeologique.evtStyle;

	this.bandeModele.hauteur=18;
	}

friseB.auto=function()
	{
	this.creer_regleA();
	this.creer_regleA_repereMa();
	this.creer_repereCenozoique();
		
	this.creer_bandeSuperEons();
	this.creer_bandeEons();
	this.creer_bandeEres();
	this.creer_bandePeriodes();
	this.creer_bandeEpoques();
	this.creer_bandeEtages();
	this.creer_bandeGlaciations();

	this.showEvt_Niveau('superEons',this.gestBandes.bandesData['superEons']);
	this.showEvt_Niveau('Eons',this.gestBandes.bandesData['Eons']);
	this.showEvt_Niveau('Eres',this.gestBandes.bandesData['Eres']);
	this.showEvt_Niveau('Periodes',this.gestBandes.bandesData['Periodes']);
	this.showEvt_Niveau('Epoques',this.gestBandes.bandesData['Epoques']);
	this.showEvt_Niveau('Etages',this.gestBandes.bandesData['Etages']);
	this.showGlaciations(this.gestBandes.bandesData['Glaciations']);

	this.creer_bandePrehistoire();
		this.showData(this.gestBandes.bandesData['Prehistoire'],prehistoire_datas['general']);
	}

// == synchronisation des bandes == //
friseB.recalcul=function()
	{
	showActivity("frise: recalcul",1);
	this.gestBandes.recalcul();
	}


// == function d'ajout dans la bande bandeRef de donnee (tableau remplit de dateTime)  ==//
friseB.showData=function(bandeRef,donnees)
	{
	showActivity("ajoute un groupe d'evt",1);
	if(typeof(bandeRef)!=='object'){showActivity("bandeRef n'est pas un objet!",1);return;}

	var global_Tfrise_msg='ajout de de la prehistoire dans la bande '+bandeRef.idHTML;
	for(var p in donnees)
		{
		if(typeof(donnees[p])=='object')
			{
			//var out=t;
			out=p;
			var EvtNu=bandeRef.evtAdd(
				donnees[p]
				,{'classe': 'evtData','opacity': 1,'zIndex': 1005}
				,{
					 deb: function(t){showActivity(global_Tfrise_msg,'add')}
					,fin: function(t){showActivity(global_Tfrise_msg,'add')}
				 }
				);
			// == application du style personnel a l'evt == //
			var attr={};
			attr.texte=p;
//			if(this.evtStyle.backgroundColor[p])attr.backgroundColor=this.evtStyle.backgroundColor[p];
			attr.click=function()
			 	{
//			 	alert('click sur evt:'+this.innerHTML,1);
				var d=document.getElementById('detailTitre');
				d.innerHTML=this.innerHTML;
			 	friseB.limiteZone(this.innerHTML);//nom de l'evt(meso,neo,etc)
				 	
			 	// creation des reperes dans la limite
			 	var deb=null;
			 	var fin=null;
			 	var repereNb=10;
			 	//this.creer_ReperesAuto(regleNom,deb,fin,repereNb,config);
			 	}
//			attr.dblclick=function()
//		 		{
//		 		alert('DOUBLE click sur evt:'+this.innerHTML,1);
//		 		}
			this.gestBandes.bandesData[bandeRef.nom].evt[EvtNu].setAttrCSS(attr);
			}
		}
	}


friseB.limiteZone=function(zone)
	{
	var niveau=null;

	for(z in eresGeologique['superEons']){if(z==zone){niveau='superEons';break;}}
	if(!niveau)for(z in eresGeologique['Eons']){if(z==zone){niveau='Eons';break;}}
	if(!niveau)for(z in eresGeologique['Eres']){if(z==zone){niveau='Eres';break;}}
	if(!niveau)for(z in eresGeologique['Periodes']){if(z==zone){niveau='Periodes';break;}}
	if(!niveau)for(z in eresGeologique['Epoques']){if(z==zone){niveau='Epoques';break;}}
	if(!niveau)for(z in eresGeologique['Etages']){if(z==zone){niveau='Etages';break;}}

	if(niveau){
	var	 deb=eresGeologique[niveau][zone].date_deb
		,fin=eresGeologique[niveau][zone].date_fin;
		this.bandeModele.limite(deb,fin);
		this.recalcul();
		this.show();
		}
	}


// == function de creation des bandes: il utilise bandeModele == //
friseB.creer_regleA=function()
	{
	showActivity("creation de la regle: Eons",1);
	var options=undefined;
	this.gestBandes.creerBande({'idHTML_parent':this.supportRegles,'idHTML': 'friseB_regleA','nom':'regleA','top':0,'hauteur':30,'title': 'regle principale (A)','genre': 'regle'},options);
	}

// == creer_ReperesAuto : creait automatique n reperes entre date_deb et date_fin  == //
// regleNom
// config: date_deb,date_fin
friseB.creer_ReperesAuto=function(regleNom,deb,fin,repereNb,config)
	{
	if(!regleNom)return null;
	if(typeof(config)!=='object')return null;
		
	var regle=this.gestBandes.bandesRegle[regleNom];
	if(!regle)return null;

	if(typeof(deb)!=='object')return null;
	if(typeof(fin)!=='object')return null;
	var deb=new TrepereTemporel_dateTime(deb);
	var fin=new TrepereTemporel_dateTime(fin);

	if(isNaN(repereNb))return null;
	var conf=config?config:{};
	var uniteNb=Math.abs(fin.diff(deb));
	if(uniteNb===0)return null;
	var distance=uniteNb/repereNb;

	var dtTemp=new TrepereTemporel_dateTime(deb);

	var classe=conf.classe?conf.classe:'';
		
	for(var regleNu=0;regleNu<repereNb;regleNu++)
		{
		var d=dtTemp.an/ut.Million;
		var evtNu=regle.evtAdd(
			 {'titre': d,'texte':d,'date_deb': new TrepereTemporel_dateTime(dtTemp)}
			,{'classe': classe}
			);
		dtTemp.addUnite(distance);
		}
	}


// -- bande regleA: echelle:Ma :  -- //
// === repere tous les 100Ma === //
friseB.creer_regleA_repereMa=function()
	{
	var regle=this.gestBandes.bandesRegle['regleA'];
	showActivity("regle: [ | ] (repere tous les 100Ma)",1);
	for(var regleNu=0;regleNu<55;regleNu++)
		{
		var an=-5*ut.Milliard+100*ut.Million*regleNu;
		var d=an/ut.Million;
		var evtNu=regle.evtAdd(
			 {'titre': d+'Ma','texte':d,'date_deb': {'an':an}}
			,{'classe': 'regleRepere'}
			);
		// == application du style personnel a l'evt == //
		var attr={};
		attr.click=function(e)
		 			{
		 			alert('click sur repere:'+e.innerHTML,1);
		 			}
		attr.dblclick=function(e)
		 			{
		 			alert('DOUBLE click sur repere:'+e.innerHTML,1);
		 			}
		this.gestBandes.bandesRegle[regle.nom].evt[regleNu].setAttrCSS(attr);
		}
	delete(a);
	}


// === repere pour le cenozoique tous les 10Ma === //
friseB.creer_repereCenozoique=function()
	{
	var regle=this.gestBandes.bandesRegle['regleA'];
	showActivity("regle: [ | ] (repere tous les 100Ma)",1);
	for(var regleNu=0;regleNu<16;regleNu++)
		{
		var an=-70*ut.Million+10*ut.Million*regleNu;
		var d=an/ut.Million;
		var evtNu=regle.evtAdd(
			 {'titre': d+'Ma','texte':d,'date_deb': {'an':an}}
			,{'classe': 'regleRepere'}
			);
		// == application du style personnel a l'evt == //
		var attr={};
		attr.click=function(e)
		 			{
		 			alert('click sur repere:'+e.innerHTML,1);
		 			}
		this.gestBandes.bandesRegle[regle.nom].evt[regleNu].setAttrCSS(attr);
		}
	//delete(a);
		}

// --Creation des bandes :  -- //
friseB.creer_bandeSuperEons=function()
	{
	showActivity("creation de la bande: superEons",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'friseB_bandeSuperEons','nom':'superEons','top': 0,'hauteur':10,'title': 'bande Super&eacute;ons'},options);
	showCode(this.gestBandes.bandesData['superEons']);
	}

friseB.creer_bandeEons=function()
	{
	showActivity("creation de la bande: Eons",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'friseB_bandeEons','nom':'Eons','top': 10,'hauteur':30,'title': 'bande Eons'},options);
	showCode(this.gestBandes.bandesData['Eons']);
	}

friseB.creer_bandeEres=function()
	{
	showActivity("creation de la bande: Eres",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'friseB_bandeEres','nom':'Eres','top': 40,'hauteur':30,'title': 'bande Eres'},options);
	showCode(this.gestBandes.bandesData['Eres']);
	}

friseB.creer_bandePeriodes=function()
	{
	showActivity("creation de la bande: Periodes",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'friseB_bandePeriodes','nom':'Periodes','top': 70,'hauteur':30,'title': 'bande Periodes'},options);
	showCode(this.gestBandes.bandesData['Periodes']);
	}

friseB.creer_bandeEpoques=function()
	{
	showActivity("creation de la bande: Epoques",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'friseB_bandeEpoques','nom':'Epoques','top': 100,'hauteur':30,'title': 'bande Epoques'},options);
	showCode(this.gestBandes.bandesData['Epoques']);
	}

friseB.creer_bandeEtages=function()
	{
	showActivity("creation de la bande: Etages",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'friseB_bandeEtages','nom':'Etages','top': 130,'hauteur':30,'title': 'bande Etages'},options);
	showCode(this.gestBandes.bandesData['Etages']);
	}


// == function d'ajout des evt ==//
friseB.showEvt_Niveau=function(niveau,bandeRef)
	{
	showActivity("ajoute un groupe d'evt",1);
	var t=eresGeologique[niveau];
	if(!t)return;
	if(typeof(bandeRef)!=='object'){showActivity("bandeRef n'est pas un objet!",1);return;}

	var global_Tfrise_msg='ajout de des eres dans la bande '+bandeRef.idHTML;
	for(var p in t)
		{
		if(typeof(t[p])=='object')
			{
			var out=t;
			out=p;
			var EvtNu=bandeRef.evtAdd(
				t[p]
				,{'classe': 'evt_'+niveau,'opacity': 1,'zIndex': 1005}
				,{
					 deb: function(t){showActivity(global_Tfrise_msg,'add')}
					,fin: function(t){showActivity(global_Tfrise_msg,'add')}
				 }
				);
			// == application du style personnel a l'evt == //
			var attr={};
			attr.texte=p;
			if(this.evtStyle.backgroundColor[p])attr.backgroundColor=this.evtStyle.backgroundColor[p];
			attr.click=function(e)
			 	{
				var d=document.getElementById('detailTitre');
				d.innerHTML=this.innerHTML;
			 	friseB.limiteZone(this.innerHTML);//nom de l'evt(meso,neo,etc)
			 	}
			attr.dblclick=function()
		 		{
		 		alert('DOUBLE click sur repere:'+e.innerHTML,1);
		 		}
			this.gestBandes.bandesData[bandeRef.nom].evt[EvtNu].setAttrCSS(attr);
			}
		}
	}

friseB.creer_bandeGlaciations=function()
	{
	showActivity("creation de la bande: Glaciations",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeGlaciations','nom':'Glaciations','top': 160,'hauteur':15,'title': 'bande Glaciations'},options);
	//showCode(this.gestBandes.bandesData['Glaciations']);
	}

// == function d'ajout des evt:Glaciation ==//
friseB.showGlaciations=function(niveau)
	{
	showActivity("ajoute un groupe d'evt",1);
	var t=Glaciations;
	if(!t)return;

	var bandeRef=this.gestBandes.bandesData['Glaciations'];
	var global_Tfrise_msg='ajout de des periodes de glaciation dans la bande '+bandeRef.idHTML;
	for(var p in t)
		{
		if(typeof(t[p].date_deb)=='object')
			{
			var out=t;
			out=p;
			var EvtNu=bandeRef.evtAdd(
				t[p]
				,{'classe': 'GlaciationEvt','opacity': 1,'zIndex': 1005}
				,{
					 deb: function(t){showActivity(global_Tfrise_msg,'add')}
					,fin: function(t){showActivity(global_Tfrise_msg,'add')}
				 }
				);
			// == application du style personnel a l'evt == //
			var attr={};
			attr.texte=p;
			//if(this.evtStyle.backgroundColor[p])attr.backgroundColor=this.evtStyle.backgroundColor[p];
			attr.backgroundColor=Glaciations.Style[t[p].type].backgroundColor;
			attr.click=function()
			 	{
				var d=document.getElementById('detailTitre');
				d.innerHTML=this.innerHTML;
			 	friseB.limiteZone(this.innerHTML);//nom de l'evt(meso,neo,etc)
			 	}
			this.gestBandes.bandesData[bandeRef.nom].evt[EvtNu].setAttrCSS(attr);
			}
		}
	}

friseB.creer_bandePrehistoire=function()
	{
	showActivity("creation de la bande: Prehistoire",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandePrehistoire','nom':'Prehistoire','top': 180,'hauteur':30,'title': 'bande Prehistoire'},options);
	}

friseB.initialisation();
friseB.auto();
return this;
}	// TfriseB()
