/*!
gestion d'une frise specialiser pour afficher les eres geologiques
Frise au limite fixe vue global
*/


friseA=null;
function createFriseA()
{
friseA=new Tfrise({
	'idHTML':'friseA_support'
	,'supportRegles':'friseA_supportRegles'
	,'supportDatas':'friseA_supportDatas'
	,'bandeModele':{'date_deb': {'an': -4.6*ut.Milliard},'date_fin': {'an': 4000},'left_marge': 0,'longueur': 1000,'top': 0,'hauteur': 30}
	});

friseA.initialisation=function()
	{
	//var init=initialisation?initialisation:{};
	// --- auteurs - creation de la bande reference/model  --- //
	showActivity("creation de la  frise",1);

	// -- initialisation des couleurs -- //
	this.evtStyle=eresGeologique.evtStyle;
	}
	
friseA.auto=function(config)
	{
	var conf=config?config:{};
		
	this.creer_regleA();
	this.creer_regleA_repereMa();

	this.creer_bandeSuperEons();
		this.showEres_Niveau('superEons',this.gestBandes.bandesData['superEons']);
	this.creer_bandeEons();
		this.showEres_Niveau('Eons',this.gestBandes.bandesData['Eons']);
	this.creer_bandeEres();
		this.showEres_Niveau('Eres',this.gestBandes.bandesData['Eres']);
	this.creer_bandePeriodes();
		this.showEres_Niveau('Periodes',this.gestBandes.bandesData['Periodes']);
	this.creer_bandeEpoques();
		this.showEres_Niveau('Epoques',this.gestBandes.bandesData['Epoques']);
	this.creer_bandeEtages();
		this.showEres_Niveau('Etages',this.gestBandes.bandesData['Etages']);

	if(conf.show)this.show();
	}


// == synchronisation des bandes == //
friseA.recalcul=function()
	{
	showActivity("frise: recalcul",1);
	this.gestBandes.recalcul();
	}

// -- function de creation des regles -- //
friseA.creer_regleA=function()
	{
	showActivity("creation de la regle: Eons",1);
	var options=undefined;
	this.gestBandes.creerBande({'idHTML_parent':this.supportRegles,'idHTML': 'regleA','nom':'regleA','top':0,'hauteur':30,'title': 'regle principale (A)','genre': 'regle'},options);
	//showCode(regle);
	}

// === repere tous les 100Ma === //
friseA.creer_regleA_repereMa=function()
	{
	var regle=this.gestBandes.bandesRegle['regleA'];
	showActivity("regle: [ | ] (repere tous les 100Ma)",1);
	for(var regleNu=0;regleNu<11;regleNu++)
		{
		var an=-5*ut.Milliard+500*ut.Million*regleNu;
		var d=an/ut.Million;
		var evtNu=regle.evtAdd(
			 {'titre': d+'Ma','texte':d,'date_deb': {'an':an}}
			,{'classe': 'regleRepere'}
			);
		// == application du style personnel a l'evt == //
		var attr={};
		attr.click=function(frise)
 			{
 			alert('click sur repere:'+null,1);
 			}
		attr.dblclick=function(frise)
 			{
 			alert('DOUBLE click sur repere:'+null,1);
 			}
		this.gestBandes.bandesRegle[regle.nom].evt[regleNu].setAttrCSS(attr);
		}
	}

friseA.creer_bandeSuperEons=function()
	{
	showActivity("creation de la bande: superEons",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeSuperEons','nom':'superEons','top': 0,'hauteur':10,'title': 'bande Super&eacute;ons'},options);
	//showCode(this.gestBandes.bandesData['superEons']);
	}

friseA.creer_bandeEons=function()
	{
	showActivity("creation de la bande: Eons",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeEons','nom':'Eons','top': 10,'hauteur':30,'title': 'bande Eons'},options);
	//showCode(this.gestBandes.bandesData['Eons']);
	}

friseA.creer_bandeEres=function()
	{
	showActivity("creation de la bande: Eres",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeEres','nom':'Eres','top': 40,'hauteur':30,'title': 'bande Eres'},options);
	//showCode(this.gestBandes.bandesData['Eres']);
	}

friseA.creer_bandePeriodes=function()
	{
	showActivity("creation de la bande: Periodes",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandePeriodes','nom':'Periodes','top': 70,'hauteur':30,'title': 'bande Periodes'},options);
	//showCode(this.gestBandes.bandesData['Periodes']);
	}

friseA.creer_bandeEpoques=function()
	{
	showActivity("creation de la bande: Epoques",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeEpoques','nom':'Epoques','top': 100,'hauteur':30,'title': 'bande Epoques'},options);
	//showCode(this.gestBandes.bandesData['Epoques']);
	}

friseA.creer_bandeEtages=function()
	{
	showActivity("creation de la bande: Etages",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeEtages','nom':'Etages','top': 130,'hauteur':30,'title': 'bande Etages'},options);
	//showCode(this.gestBandes.bandesData['Etages']);
	}

// == function d'ajout des evt:Eres ==//
friseA.showEres_Niveau=function(niveau,bandeRef)
	{
	showActivity("ajoute un groupe d'evt",1);
	var t=eresGeologique[niveau];
	if(!t)return;
	if(typeof(bandeRef)!=='object'){showActivity("bandeRef n'est pas un objet!",1);return;}

	global_Tfrise_msg[this.idHTML]='ajout de des eres dans la bande '+bandeRef.idHTML;
	for(var p in t)
		{
		if(typeof(t[p])=='object')
			{
			var out=t;
			out=p;
			var EvtNu=bandeRef.evtAdd(
				t[p]
				,{'classe': 'evt_'+niveau,'opacity': 1,'zIndex': 1005}
				,{
					 deb: function(t){showActivity(global_Tfrise_msg[this.idHTML],'add')}
					,fin: function(t){showActivity(global_Tfrise_msg[this.idHTML],'add')}
				 }
				);
			// == application du style personnel a l'evt == //
			var attr={};
			attr.texte=p;
			if(this.evtStyle.backgroundColor[p])attr.backgroundColor=this.evtStyle.backgroundColor[p];
			attr.click=function()
			 	{
//			 	alert('click sur evt:'+this.innerHTML,1);
				var d=document.getElementById('detailTitre');
				d.innerHTML=this.innerHTML;
			 	//if(typeof(friseB.limiteZone)=='function')
			 		friseB.limiteZone(this.innerHTML);//nom de l'evt(meso,neo,etc)
				 	
			 	// creation des reperes dans la limite
			 	var deb=null;
			 	var fin=null;
			 	var repereNb=10;
			 	//this.creer_ReperesAuto(regleNom,deb,fin,repereNb,config);
			 	}
			attr.dblclick=function()
		 		{
		 		alert('DOUBLE click sur evt:'+this.innerHTML,1);
		 		}
			this.gestBandes.bandesData[bandeRef.nom].evt[EvtNu].setAttrCSS(attr);
			}
		}
	}
friseA.initialisation();
return this;
}	// createFriseA()
